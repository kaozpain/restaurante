<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'PrincipalController@index')->name('principal');
Route::get('/mesasInfo', 'MesasInController@index')->name('mesas-info');
Route::get('/estadisticas', 'EstadisticasController@index')->name('estadisticas');
Route::get('/menu-avanzado', 'MenuAvanzadoController@index')->name('menu-avanzado');
Route::get('/menu-empleados', 'MenuEmpleadosController@index')->name('menu-empleados');
Route::get('/menu-mesas', 'MenuMesasController@index')->name('menu-mesas');
Route::get('/menu-principal', 'MenuPrincipalController@index')->name('menu-principal');
Route::get('/vista-mesa/{id_mesa}/mesa/{nombre}', 'MesasInController@getPopUpVista')->name('vista-mesa');
Route::get('/menu-avanzado/{tipo}/{nombre}', 'MenuAvanzadoController@getVistaAgregarEdicion')->name('{tipo}');
Route::get('/mesas-posiciones', 'MesasInController@VerPosicionesDeMesas')->name('mesas-posiciones');
Route::post('/mesas-posiciones/guardar', 'MesasInController@GuardarPosicionMesas');
Route::get('/plato/{id_plato?}/ver-descripcion/{id_mesa?}', 'MenuPrincipalController@getDescripcionPlatoOPromocion')->name('plato-info');
Route::post('/plato/guardar', 'MenuPrincipalController@guardarPlato');
Route::get('/platos/{es_menu?}/ver-todos/{id_mesa?}', 'MenuPrincipalController@getPlatosActivos');
Route::post('/plato/agregar', 'MenuPrincipalController@vistaAgregarPlato');
Route::post('/orden/guardar', 'OrdenesController@guardarOrden')->name('asdasd');
Route::get('/platos/vista-promocion/{es_menu?}/{id_menu?}', 'MenuPrincipalController@getVistaPromociones');
Route::get('/platos/vista-platos/{id_categoria?}/{es_menu?}/{id_menu?}', 'MenuPrincipalController@getVistaPlatosPorCategorias');
Route::post('/guardar/{tipo}/{id}/{cambiarEstado?}', 'MenuAvanzadoController@guardarAvanzado');
Route::get('/pruebas/{funcion?}', 'PruebasController@index');
Route::post('/obtenerMateriasPrimasParaGuarnicion', 'MenuAvanzadoController@agregarMateriaPrimaAVistaGuarnicion');
Route::post('/guardar/imagen', 'StorageController@guardarArchivo');
