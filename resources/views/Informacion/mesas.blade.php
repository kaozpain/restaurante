<div class="container col-12 col-lg-3" style="float: left; height: 100%; overflow: scroll">

    <p><br>
    @if(!empty($sin_stock))
        <div>
            <!--h2 style="text-align: center">Productos Fuera de stock</h2-->
            <table class="table table-striped table-dark" style="width: 100%; border-collapse: separate;">
                <caption style=" color: #fff; background: #000; caption-side: top; padding: 1.1em; text-align: center;font-size: 1.5em">Productos fuera de stock</caption>
                <thead>
                    <th style="width: 70%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em">NOMBRE</th>
                    <th style="width: 30%; color: black; background: deepskyblue;border: 1px solid black; font-size: 0.8em">FECHA TERMINACIÒNN</th>
                </thead>
                <tbody>
                    @foreach($sin_stock as $sinStock)
                        <tr style="color: white; background: deepskyblue;border: 1px solid black">
                            <td style="color: black; background: whitesmoke;border: 1px solid black"> {{ $sinStock['nombre'] }}</td>
                            <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $sinStock['fecha_terminado'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
    </br></p>
    @if(!empty($Poco_stock))
        <div>
            <p>
            <!--h2 style="text-align: center">Productos Apunto de acabar</h2-->
            <table class="table table-striped table-dark" style="border-collapse: separate">
                <caption style=" color: #fff; background: #000; caption-side: top; padding: 1.1em; text-align: center;font-size: 1.5em">Productos a punto de acabar</caption>
                <thead>
                <th style="width: 70%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em">NOMBRE</th>
                <th style="width: 30%;color: black; background: deepskyblue;border: 1px solid black">CANTIDAD</th>
                </thead>
                <tbody>
                    @foreach($Poco_stock as $pocoStock)
                    <tr style="color: white; background: deepskyblue;border: 1px solid black">
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $pocoStock['nombre'] }}</td>
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $pocoStock['cantidad'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </p>
        </div>
    @endif
    <p><br>
    <!--h2 style="text-align: center">Ventas Hoy 123</h2-->
    @if(!empty($ventas))
        <div style="overflow-x: scroll">
            <table class="table table-striped table-dark" style="width: 100%; border-collapse: separate">
                <caption style=" color: #fff; background: #000; caption-side: top; padding: 1.1em; text-align: center;font-size: 1.5em">Ventas del dìa</caption>
                <thead>
                <tr style="color: white; background: deepskyblue;border: 1px solid black">
                    <th style="width: 50%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">NOMBRE</th>
                    <th style="width: 15%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">CANTIDAD</th>
                    <th style="width: 15%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">P/U</th>
                    <th scope="col"; style="color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">P/T</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($ventas as $venta)
                    <tr>
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $venta['nombre'] }}</td>
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $venta['cantidad'] }}</td>
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $venta['precio'] }}</td>
                        <td style="color: black; background: whitesmoke;border: 1px solid black">{{ $venta['precio_final'] }}</td>
                    </tr>
                    @endforeach
                <tr class="bg-success">
                    <th style="color: black; background: lawngreen;border: 1px solid black"colspan="3">TOTAL</th>
                    <td style="border-left: double; color: black; background: lawngreen;border: 1px solid black">{{ $suma }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @endif
    </br></p>
</div>
<div>
    <div class="col-lg-9 col-md-12" style="border: dashed; height: 100%; overflow: scroll">
        <?= $MesasInController->index() ?>
    </div>
</div>
@foreach($scriptsJs as $scriptJs )
    <?=  $scriptJs  ?>
@endforeach