@include('Principales.header')
@section('content')
@stop
@foreach($scriptsJs as $scriptJs )
    <?=  $scriptJs  ?>
@endforeach
<div class="container" style="padding-top: 5%; height: 100%">
    <div class="text-lg-center text-center">
        <h3><b><label style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 40px;">TITULO PARA GRAFICA</label></b></h3>
    </div>
    <div class="col-lg-3" style="float: left; margin-top: 15%">
        <table class="table table-striped table-dark" style="width: 100%; border-collapse: separate">
            <thead>
                <th style="width: 70%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">NOMBRE</th>
                <th style="width: 30%;color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">CANTIDAD</th>
            </thead>
            <tbody>
                <tr>
                    <td style="color: black; background: whitesmoke;border: 1px solid black"> nombre</td>
                    <td style="color: black; background: whitesmoke;border: 1px solid black">20</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-9" style="float: right; padding: 10%" >
    <canvas id="myChart" width="400" height="400"></canvas>
    </div>
<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange", 'Gray'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3, 15],
                backgroundColor: [
                    <?php foreach ($color_array as $color){ ?>
                    color_convert.to_rgba_opacity( '<?= $color ?>', '0.2'),
                    <?php }; ?>
                ],
                borderColor: [
                    <?php foreach ($color_array as $color){ ?>
                    color_convert.to_rgba( '<?= $color ?>'),
                    <?php }; ?>
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>
</div>

<div class="container" style="padding-top: 5%; height: 100%">
    <div class="text-lg-center text-center">
        <h3><b><label style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 40px;">TITULO PARA GRAFICA</label></b></h3>
    </div>
    <div class="col-lg-3" style="float: left; margin-top: 15%">
        <table class="table table-striped table-dark" style="width: 100%; border-collapse: separate">
            <thead>
            <th style="width: 70%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">NOMBRE</th>
            <th style="width: 30%;color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">CANTIDAD</th>
            </thead>
            <tbody>
            <tr>
                <td style="color: black; background: whitesmoke;border: 1px solid black"> nombre</td>
                <td style="color: black; background: whitesmoke;border: 1px solid black">20</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-9" style="float: right; padding: 10%" >
        <canvas id="myChart2" width="400" height="400"></canvas>
    </div>
    <script>
        var ctx = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange", 'Gray'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3, 15],
                    backgroundColor: [
                        <?php foreach ($color_array as $color){ ?>
                        color_convert.to_rgba_opacity( '<?= $color ?>', '0.2'),
                        <?php }; ?>
                    ],
                    borderColor: [
                        <?php foreach ($color_array as $color){ ?>
                        color_convert.to_rgba( '<?= $color ?>'),
                        <?php }; ?>
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                }
            }
        });
    </script>
</div>

<div class="container" style="padding-top: 5%; height: 100%">
    <div class="text-lg-center text-center">
        <h3><b><label style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 40px;">TITULO PARA GRAFICA</label></b></h3>
    </div>
    <div class="col-lg-3" style="float: left; margin-top: 15%">
        <table class="table table-striped table-dark" style="width: 100%; border-collapse: separate">
            <thead>
            <th style="width: 70%; color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">NOMBRE</th>
            <th style="width: 30%;color: black; background: deepskyblue;border: 1px solid black; font-size: 1.1em" scope="col">CANTIDAD</th>
            </thead>
            <tbody>
            <tr>
                <td style="color: black; background: whitesmoke;border: 1px solid black"> nombre</td>
                <td style="color: black; background: whitesmoke;border: 1px solid black">20</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-9" style="float: right; padding: 10%" >
        <canvas id="myChart3" width="400" height="400"></canvas>
    </div>
    <script>
        var ctx = document.getElementById("myChart3").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange", 'Gray'],
                datasets: [{
                    label: '# of Votes',
                    data: [{
                        x: 10,
                        y: 5
                    }, {
                        x: 15,
                        y: 20
                    },{
                        x: 5,
                        y: 10
                    }],
                    borderColor: [
                        <?php foreach ($color_array as $color){ ?>
                        color_convert.to_rgba( '<?= $color ?>'),
                        <?php }; ?>
                    ],
                    borderWidth: 5
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    </script>
</div>