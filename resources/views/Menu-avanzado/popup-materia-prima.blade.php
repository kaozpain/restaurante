<div class="modal-dialog" role="document" style="max-width: auto">
    <div class="modal-content">
        <div class="modal-header">
            <h3><label for="exampleInputEmail1" style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 25px;">MATERIA PRIMA</label></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="POST" action="guardar/{{$tipo}}/{{$id}}">
            {{ csrf_field() }}
              <p>
                <fieldset>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="col-12" style="float: left">
                                <label for="nombre" style="font-size: 18px; font-weight: bold">Nombre:</label>
                                <input type="text" maxlength="20" class="form-control" name="nombre" aria-describedby="emailHelp"
                                       placeholder="Ingrese nombre Completo">
                            </div>
                        </div>
                    </div>
                </fieldset>
                <p>
                <fieldset>
                    <div class="col-12">
                    <div class="form-group">
                        <div class="col-12" style="float: left">
                            <label for="cantidad" style="font-size: 18px; font-weight: bold">Cantidad inicial:</label>
                            <input type="number" min="0" class="form-control" name="cantidad" placeholder="Ingrese cantidad en stock Actual">
                        </div>
                    </div>
                    </div>
                </fieldset>
                </p>

                <fieldset>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="col-12" style="float: left">
                                <label for="medicion_id" style="font-size: 18px; font-weight: bold">Medicion:</label>
                                  <select name="medicion_id" class="custom-select mr-5">
                                    <option selected>Escoja el valor de medida:</option>
                                      @foreach($mediciones as $medicion)
                                          <option value="{{$medicion['id']}}">{{$medicion['nombre_abreviatura']}}</option>
                                      @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <p>
                <fieldset>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="col-12" style="float: left">
                                <label for="cantidad_alerta" style="font-size: 18px; font-weight: bold">Cantidad alerta:</label>
                                <input type="number" min="0" class="form-control" name="cantidad_alerta" placeholder="Indicar cantidad para alerta de producto a acabarse">
                            </div>
                        </div>
                    </div>
                </fieldset>
                </p>
            <div class="col-12">
                <div class="align-content-center form-check" style="clear: both">
                    <div class="col-lg-4 col-12" style="...">
                    <input type="checkbox" class="form-check-input" name="activo" checked>
                    <label class="form-check-label" for="activo">Activo</label>
                </div>
                <p><fieldset style="text-align: center"><button type="submit" class="btn btn-default" style="background-color: #1883ba; color: #ffffff;">GUARDAR</button></></fieldset></p>
            </div>
        </form>
    </div>
</div>