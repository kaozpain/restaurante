<style>
    .disabled-select {
        background-color:#d5d5d5;
        opacity:0.5;
        border-radius:3px;
        cursor:not-allowed;
        pointer-events: none;
    }
</style>
<div id="materia_prima_{{$id_unico}}" style="clear: both; padding-bottom: 40px" class="container col-12">
    <label for="materias_primas[id]" style="font-size: 18px; font-weight: bold">Materia Prima:</label>
    <div class="container">
        <div>
            <select name="materias_primas[{{$id_unico}}][id]" class="form-control col-11 js-example-basic-usage">
            </select>
        </div>
        <input type="number" class="form-control col-6" style="float: left" name="materias_primas[{{$id_unico}}][cantidad_materia_prima]" placeholder="Elija la cantidad aproximada de uso"/>
        <i class="material-icons col-1" style="float: right; cursor: pointer" onclick="menuAvanzado.borrarMateriPrima(materia_prima_{{$id_unico}});">delete</i>
    </div>
</div>

<script>
    $(document).ready(function () {
        let datos=[
                @foreach($materias_primas as $materia_prima)
            {id: '{{$materia_prima['id']}}',
                abreviatura: '{{$materia_prima['medicion']['abreviatura']}}',
                text: '{{$materia_prima['nombre']}}'+' - '+'{{$materia_prima['medicion']['abreviatura']}}'},
            @endforeach
        ];
        $('.js-example-basic-usage').select2({
            data: datos
        });
    });
</script>