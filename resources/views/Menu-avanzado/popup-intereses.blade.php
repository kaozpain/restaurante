<div class="modal-dialog" role="document" style="max-width: auto">
    <div class="modal-content">
        <div class="modal-header">
            <h3><label for="exampleInputEmail1" style="font-family:Trebuchet MS,Comic Sans MS,arial,Verdana,Sans-serif; color: blue; font-size: 25px;">INTERESES</label></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div>
        <form method="POST" action="guardar/{{$tipo}}/{{$id}}">
            {{ csrf_field() }}
            <p>
                <fieldset>
                <div class="col-12">
                    <div class="form-group">
                        <div class="col-12" style="float: left">
                            <label for="nombre" style="font-size: 18px; font-weight: bold" >Nombre:</label>
                            <input type="text" class="form-control" name="nombre" aria-describedby="emailHelp" placeholder="Nombre Interes">
                        </div>
                    </div>
                </div>
                </fieldset>
            </p>
            <p>
            <fieldset>
                <div class="col-12">
                    <div class="form-group">
                        <div class="col-12" style="float: left">
                            <label for="porcentaje" style="font-size: 18px; font-weight: bold">Porcentaje</label>
                            <input type="number" max="100" min="0" class="form-control" name="porcentaje" placeholder="Porcentaje">
                        </div>
                    </div>
                </div>
            </fieldset>
            </p>
                <div class="col-12">
                    <div class="align-content-center form-check" style="clear: both">
                        <div class="col-lg-4 col-12" style="...">
                            <input type="checkbox" class="form-check-input" name="activo" checked>
                            <label class="form-check-label" for="activo">Activo</label>
                        </div>
                         <p><fieldset style="text-align: center"><button type="submit" class="btn btn-default" style="background-color: #1883ba; color: #ffffff;">GUARDAR</button></></fieldset></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>