<thead>
<tr>
    <?php $contador=0; ?>
    @foreach($tabla as $rows)
            <th><span style="cursor: pointer">{{ucwords(str_replace('_',' ',$rows))}}<i class="material-icons">@if($contador==0) expand_more @else expand_less @endif </i></span></th>
    @endforeach
    <th></th>
</tr>
</thead>