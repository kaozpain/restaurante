<div>
    <div class="table-wrapper" style="margin: auto">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-4">
                    <h2><b>{{$titulo}}</b></h2>
                </div>
            </div>
        </div>
        <div class="table-filter">
            <div class="row">
                <div class="col-sm-9">
                    <button type="button" class="btn btn-primary" ><i class="material-icons">find_in_page</i></button>
                    <div class="filter-group">
                        <label>Nombre</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="filter-group">
                        <label>Location</label>
                        <select class="form-control">
                            <option>All</option>
                            <option>Berlin</option>
                            <option>London</option>
                            <option>Madrid</option>
                            <option>New York</option>
                            <option>Paris</option>
                        </select>
                    </div>
                    <div class="filter-group">
                        <label>Status</label>
                        <select class="form-control">
                            <option>Any</option>
                            <option>Activos</option>
                            <option>No Activos</option>
                        </select>
                    </div>
                    <span class="filter-icon"><i class="fa fa-filter"></i></span>
                </div>
            </div>
        </div>
        <div class="row" style="float: right; margin-right: 5%;">
            <td><a style="vertical-align: middle" href="#" onclick="menuAvanzado.popup_vista_avanzada('{{$ver_vista}}', 0);" class="view" title="Agregar{{$titulo}}" data-toggle="tooltip"><span>Agregar{{$titulo}}<i class="material-icons" style="vertical-align: bottom">add</i></span></a></td>
        </div>
        <table class="table table-striped table-hover">
            <?= $Controller->GetVistaHeader($tipo); ?>
            <tbody>
                <div id="vista_fila">
                    <?= $Controller->GetVistaFila($tipo); ?>
                </div>
            </tbody>
        </table>

                <div class="clearfix">
                    <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                    <ul class="pagination">
                        <li class="page-item disabled"><a href="#">Previous</a></li>
                        <li class="page-item Active"><a href="#" class="page-link">1</a></li>
                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                        <li class="page-item"><a href="#" class="page-link">3</a></li>
                        <li class="page-item "><a href="#" class="page-link">4</a></li>
                        <li class="page-item"><a href="#" class="page-link">5</a></li>
                        <li class="page-item"><a href="#" class="page-link">6</a></li>
                        <li class="page-item"><a href="#" class="page-link">7</a></li>
                        <li class="page-item"><a href="#" class="page-link">Next</a></li>
                    </ul>
                </div>
    </div>
</div>