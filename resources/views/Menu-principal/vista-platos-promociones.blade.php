@if($promociones)
    <h1> PROMOCIONES </h1>
    <div class="grid-container" style="padding: 20px">
@endif
    @foreach($promociones as $promocion)
{{--        Muestra en caso de que sea mayor a sm (Pantalla celulares)        --}}
        <div class="d-none d-md-none d-lg-block">
            <button class="grid-item" style="height: 170px; text-align: center; width: 100%"
                    onclick="@if($esMenu) platos.vista_agregar_plato({{$promocion['id']}},'promo'); @else platos.vista_platos({{$promocion['id']}},{{ $id_mesa }}); @endif ">
                <div style="height: 60%; padding: 5%">
                    <img src="http://www.restaurantelatranquera.com/wp-content/uploads/2014/10/parrillapara2.png"
                         height="100%" width="100%">
                </div>
                <div style="height: 40%; overflow: hidden; display: flex;justify-content: center;align-items: center;">
                    <span>{{ $promocion['nombre'] }}</span>
                </div>
            </button>
        </div>

        {{--         Muestra en caso de que sea mayor a md (Pantalla celulares)--}}
        <div class="d-block d-lg-none ">
            <button class="grid-item" style="height: 170px; text-align: center; width: 100%"
                    onclick="@if($esMenu) platos.popup_vista_agregar_plato({{$promocion['id']}},'promo'); @else platos.vista_platos({{$promocion['id']}},{{ $id_mesa }}); @endif">
                <div style="height: 60%; padding: 5%">
                    <img src="http://www.restaurantelatranquera.com/wp-content/uploads/2014/10/parrillapara2.png"
                         height="100%" width="100%">
                </div>
                <div style="height: 40%; overflow: hidden; display: flex;justify-content: center;align-items: center;">
                    <span>{{ $promocion['nombre'] }}</span>
                </div>
            </button>
        </div>
    @endforeach
</div>