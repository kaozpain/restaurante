<div class="modal-dialog" role="document" style="max-width: 80%; pointer-events: unset">
    <div class="modal-content">
        <div class="col-11">
            <form action="plato/guardar" method="post">
                {{ csrf_field() }}
                @if($nuevo)
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Escoger tipo de producto a agregar</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="plato_radio" value="plato" id="plato_radio_plato" onclick="platos.vista_resgistro_platos('plato')" checked>
                                <label class="form-check-label" for="plato_radio">
                                    Plato
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="plato_radio" value="promo" id="plato_radio_promo" onclick="platos.vista_resgistro_platos('promo')">
                                <label class="form-check-label" for="plato_radio">
                                    Promocion
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                @endif
                <div id="formulario_agregar_plato" @if(!$plato_activo) style="display: none" @endif>
                    <?= $Controller->getVistaTipoAgregar('plato'); ?>
                </div>
                <div id="formulario_agregar_promo" @if($plato_activo) style="display: none" @endif>
                    <?= $Controller->getVistaTipoAgregar('promo'); ?>
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
        </div>
    </div>
</div>