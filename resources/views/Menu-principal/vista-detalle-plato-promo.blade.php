<div class="modal-dialog modal-lg" role="document" style="max-width: 80%">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">close</i></span>
            </button>
        </div>
        <div class="col-12">
            <div style="width: 80%">
                <img src="https://http2.mlstatic.com/mesa-cairo-madera-para-restaurantes-cafeterias-pizzerias-D_NQ_NP_272001-MLM20251580214_022015-O.jpg"
                     width="100%">
            </div>
            <div>
                <h1>Orden {{$id_mesa}}</h1>
                <h1>{{ $plato['nombre'] }}</h1>
                <p style="text-align: justify">
                    <a>Precio: <span>{{ $plato['precio'] }}</span></a>
                    </br>
                    <a>Detalle: <span>{{ $plato['detalle'] }}</span></a>
                </p>
            </div>
{{--         validacion pedidos   --}}
            <form method="POST" action="{{ url('orden/guardar') }}" style="pointer-events: all">
                {{ csrf_field() }}
                <input type="hidden" name="id_mesa" value="{{ $id_mesa }}">
                <input type="hidden" name="id_plato" value="{{$plato['id']}}"/>
                <input type="number" class="form-control" name="cantidad_plato" id="pedido_mesa" min="1" max="50" value="1" onchange="platos.cambiar_cantidad_guarnicion(this)">
                <div id="guarniciones_plato">
                @foreach($plato['Guarniciones'] as $key =>$guarniciones)
                        <span>{{ $guarniciones['nombre'] }}</span>
                        <input type="number" class="form-control" name="guarnicion['{{$guarniciones['id']}}']['cantidad']" id="guarnicion_{{$guarniciones['id']}}" min="0" max="50" onmouseenter="platos.validar_guardicion()" onclick="platos.validar_guardicion()" @if($key==0) value="1" @else value="0" @endif>
                @endforeach
                </div>
                <button type="submit">Enviar</button>
                <a type="button" onclick="platos.recargar_menu({{$id_mesa}}, 0)">Cerrar</a>
            </form>
        </div>
    </div>
</div>