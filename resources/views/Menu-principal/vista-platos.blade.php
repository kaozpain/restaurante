<style>
    @media (max-width: 768px) {
        .grid-container {
            display: grid;
            grid-gap: 2%;
            grid-template-columns: repeat(3, 30%);
        }
    }
    @media (min-width: 769px) {
        .grid-container {
            display: grid;
            grid-gap: 1%;
            grid-template-columns: repeat(5, 17%);
        }
    }
</style>
<div style="margin: 10px">
    <select class="js-example-responsive" id="buscador_categorias" style="width: 75%"
            onchange="platos.vista_platos_busqueda({{$esMenu}}, {{ $id_mesa }})">
        <option value="0" selected>
            Todas las Categorias
        </option>
        @if($mostrar_promo)
            <option value="-2">
                Promociones
            </option>
        @endif
        @foreach($categorias as $categoria)
            <option value="{{$categoria['id']}}">
                {{$categoria['nombre']}}
            </option>
        @endforeach
        @if($esMenu)
            <option value="-1">
                No Asignados
            </option>
        @endif
    </select>
</div>

<div class="grid-container" style="padding: 20px">
    @if($esMenu)
        <div class="d-none d-md-none d-lg-block contanier">
            <button class="grid-item" style="height: 170px; text-align: center; width: 100%" onclick="platos.vista_agregar_plato(0, 'new')">
                <div style="height: 60%; padding: 5%">
                    <img src="https://i.pinimg.com/236x/47/f4/0f/47f40f52dd0e9531b4e2ca343baddc8b.jpg" height="100%" width="100%">
                </div>
                <div style="height: 40%; overflow: hidden; display: flex;justify-content: center;align-items: center;">
                    <span>Agregar Al Menu</span>
                </div>
            </button>
        </div>
        <div class="d-block d-lg-none contanier">
            <button class="grid-item" style="height: 170px; text-align: center"
                    onclick="platos.popup_vista_agregar_plato(0, 'new')">
                <div style="height: 60%; padding: 5%">
                    <img src="https://i.pinimg.com/236x/47/f4/0f/47f40f52dd0e9531b4e2ca343baddc8b.jpg" height="100%" width="100%">
                </div>
                <div style="height: 40%; overflow: hidden; display: flex;justify-content: center;align-items: center;">
                    <span>Agregar Al Menu</span>
                </div>
            </button>
        </div>
    @endif
</div>


<div id="vista-categorias-promociones">
    <?= $Controller->getVistaPromociones($esMenu, $id_mesa) ?>
</div>
<div id="vista-categorias-platos">
    <?= $Controller->getVistaPlatosPorCategorias(0, $esMenu, $id_mesa) ?>
</div>

@foreach($scriptsJs as $scriptJs )
    <?=  $scriptJs  ?>
@endforeach

<script async>
    $(document).ready(function () {
        $('#buscador_categorias').select2();
    });
</script>