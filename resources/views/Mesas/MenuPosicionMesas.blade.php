<div class="col-12" style="float: inline-end; overflow-y: scroll; height: 100%; cursor: default">
<?php $cont=1; ?>
@for($i=0; $i<$fila; $i++)
    <div class="col-11">
        @for($j=0; $j<$columna; $j++)
            <div id="MesaDiv_{{ $i }}_{{ $j }}" style="float: left; width: {{ $tamanio_vertical }}%; max-width: 30%; text-align: center;" data-toggle="modal">
                <?php if($creacionMesas) $mesasTodas[$i][$j]['activo']=1; ?>
                <div style="visibility: <?php if(empty($mesasTodas[$i][$j]['activo'])) { echo 'hidden'; }?>" >
                    <img style="width: 80%; cursor: pointer " src="{{ asset('imagenes/sistema/mesas.jpg') }}" onClick= "@if($esMenu) mesas.habilitarDeshabilitarMesa({{ $i }},{{ $j }}) @else mesas.popup_vista_mesas(<?php if(!empty($mesasTodas[$i][$j]['id'])) echo $mesasTodas[$i][$j]['id']; else echo 0; ?>, <?php if(!empty($mesasTodas[$i][$j]['nombre'])) echo "'".$mesasTodas[$i][$j]['nombre']."'"; else echo 0; ?>) @endif" />
                    <div>
                    @if($esMenu)
                        <input name="Mesa[{{ $i }}][{{ $j }}][nombre]" style="width: 80%; border: none; text-align: center;" maxlength="10" type="text" value="<?php if(empty($mesasTodas[$i][$j]['nombre'])) echo "mesa ".$cont++; else echo $mesasTodas[$i][$j]['nombre']?>" />
                        <input type="hidden" name="Mesa[{{ $i }}][{{ $j }}][activo]" id="habilitadoDeshabilitado_{{ $i }}_{{ $j }}" value="<?php if(empty($mesasTodas[$i][$j]['activo'])) echo 0; else echo $mesasTodas[$i][$j]['activo']?>">
                    @else
                        <span id="Mesa_{{ $i }}_{{ $j }}" style="width: 80%; border: none; text-align: center;"><?php if(empty($mesasTodas[$i][$j]['nombre'])) echo "mesa ".$cont++; else echo $mesasTodas[$i][$j]['nombre']?></span>
                    @endif
                    </div>
                </div>
            </div>
        @endfor
        <div style="clear: both"></div>
    </div>
@endfor
</div>
@if($esMenu)
    <button type="submit">asdasdas</button>
@endif