<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablasMesas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $this->crearMesas();
        print_r('Creacion de tabla mesas exitoso');
        $this->crearMesasHistoria();
        print_r('Creacion de tabla mesas historias exitoso');
        $this->crearCuentas();
        print_r('Creacion de tabla cuentas exitoso');
        $this->crearPedidos();
        print_r('Creacion de tabla pedidos exitoso');
        $this->crearClientes();
        print_r('Creacion de tabla clientes exitoso');

    }

    public function crearMesas(){
        Schema::create('mesas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('posicion_x');
            $table->integer('posicion_y');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }


    public function crearMesasHistoria(){
        Schema::create('mesas_historia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mesa_id');
            $table->integer('personas');
            $table->boolean('multiples_cuentas');
            $table->integer('usuario_id');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }


    public function crearCuentas(){
        Schema::create('cuentas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mesa_historia_id');
            $table->integer('cliente_id');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }


    public function crearPedidos(){
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cuenta_id');
            $table->string('class');
            $table->integer('class_id');
            $table->integer('cantidad');
            $table->string('detalle')->nullable();
            $table->boolean('entregado');
            $table->boolean('activo');
        });
    }


    public function crearClientes(){
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula');
            $table->string('nombre');
            $table->date('fecha_nacimiento');
            $table->string('direccion')->nullable();
            $table->string('correo')->nullable();
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mesas');
        Schema::dropIfExists('mesas_historia');
        Schema::dropIfExists('cuentas');
        Schema::dropIfExists('pedidos');
        Schema::dropIfExists('clientes');
    }
}
