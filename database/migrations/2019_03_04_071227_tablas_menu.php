<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablasMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->creacionMenu();
        print_r('Creacion de tabla menu exitoso');
        $this->creacionTipoMenu();
        print_r('Creacion de tabla tipo menu exitoso');
        $this->creacionMenuGuarnicion();
        print_r('Creacion de tabla menu guarnicion exitoso');
        $this->creacionGuarnicion();
        print_r('Creacion de tabla guarnicion exitoso');
        $this->creacionReceta();
        print_r('Creacion de tabla receta exitoso');
        $this->creacionMateriaPrima();
        print_r('Creacion de tabla materia prima exitoso');
        $this->creacionMedicion();
        print_r('Creacion de tabla medicion exitoso');
        $this->creacionIntereses();
        print_r('Creacion de tabla intereses exitoso');
        $this->creacionPedidosGuarnicion();
        print_r('Creacion de tabla pedidos guarnicion exitoso');
        $this->creacionImagen();
        print_r('Creacion de tabla imagen exitoso');
    }

    public function creacionMenu(){
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->integer('tipo_menu_id')->nullable();
            $table->boolean('tiene_guarnicion');
            $table->double('precio');
            $table->string('detalle')->nullable();
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    public function creacionTipoMenu(){
        Schema::create('tipo_menu', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->integer('padre_id');
            $table->boolean('activo');
        });
    }

    public function creacionGuarnicion(){
        Schema::create('guarnicion', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->integer('tipo_menu_id')->nullable();
            $table->double('precio');
            $table->string('detalle')->nullable();
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    public function creacionMenuGuarnicion(){
        Schema::create('menu_guarnicion', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('guarnicion_id');
            $table->double('precio_adicional')->nullable();
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    public function creacionReceta(){
        Schema::create('receta', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('class');
            $table->integer('class_id');
            $table->integer('materia_prima_id');
            $table->double('cantidad_materia_prima');
            $table->boolean('activo');
        });
    }

    public function creacionMateriaPrima(){
        Schema::create('materia_prima', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->integer('medicion_id');
            $table->double('cantidad');
            $table->double('cantidad_alerta');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    public function creacionMedicion(){
        Schema::create('medicion', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->string('abreviatura');
        });
    }

    public function creacionIntereses(){
        Schema::create('intereses', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('nombre');
            $table->double('porcentaje');
            $table->boolean('activo');
        });
    }


    public function creacionPedidosGuarnicion(){
        Schema::create('pedidos_guarnicion', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('pedidos_id');
            $table->integer('guarnicion_id');
        });
    }

    public function creacionImagen(){
        Schema::create('pedidos_guarnicion', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('class');
            $table->integer('class_id');
            $table->string('ruta');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
        Schema::dropIfExists('tipo_menu');
        Schema::dropIfExists('guarnicion');
        Schema::dropIfExists('receta');
        Schema::dropIfExists('materia_prima');
        Schema::dropIfExists('medicion');
        Schema::dropIfExists('intereses');
    }
}
