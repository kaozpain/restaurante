<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablasPromos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->crearPromos();
        print_r('Creacion de tabla promociones exitoso');
        $this->crearMenuPromos();
        print_r('Creacion de tabla platos promociones exitoso');
    }

    public function crearPromos(){
        Schema::create('promociones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('imagen')->nullable();
            $table->string('detalle')->nullable();
            $table->double('precio');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }


    public function crearMenuPromos(){
        Schema::create('menu_promociones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promociones_id');
            $table->string('class');
            $table->integer('class_id');
            $table->integer('cantidad');
            $table->integer('incluye_guarnicion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('promociones');
        Schema::dropIfExists('menu_promociones');
    }
}
