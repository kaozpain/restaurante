<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablasUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->creacionUsuario();
        print_r('Creacion de tabla usuario exitoso');
        $this->creacionTipoUsuario();
        print_r('Creacion de tabla tipo_usuario exitoso');
        $this->creacionPermisos();
        print_r('Creacion de tabla Permiso exitoso');
        $this->CreacionReseteoClave();
        print_r('Creacion de tabla resetPass exitoso');
    }

    public function creacionUsuario(){
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('login')->unique();
            $table->string('clave');
            $table->integer('tipo_usuario_id');
            $table->string('nombre');
            $table->rememberToken();
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_modificacion')->nullable();
            $table->boolean('activo');
        });
    }

    public function creacionTipoUsuario(){
        Schema::create('tipo_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->boolean('activo');
        });
    }

    public function creacionPermisos(){
        Schema::create('permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_usuario_id');
            $table->string('permiso');
        });
    }

    public function CreacionReseteoClave(){
        Schema::create('reseteo_clave', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('login');
            $table->string('token');
            $table->timestamp('fecha_creacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
        Schema::dropIfExists('tipo_usuario');
        Schema::dropIfExists('permisos');
        Schema::dropIfExists('reseteo_clave');
    }
}
