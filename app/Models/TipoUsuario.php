<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends GeneralModel
{
    //
    protected $table='tipo_usuario';
    protected $fillable = ['nombre', 'activo'];

}
