<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PedidosGuarnicion extends GeneralModel
{
    //
    protected $table='pedidos_guarnicion';
    protected $fillable = ['pedido_id', 'guarnicion_id'];


    public function Guarnicion(){
        return $this->belongsTo(Guarnicion::class, 'guarnicion_id','id');
    }
}
