<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;

class Usuarios extends GeneralModel
{
    use Notifiable;

    protected $table='usuarios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'login', 'clave', 'tipo_usuario_id', 'activo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'clave', 'remember_token',
    ];

    public function getAuthPassword()
    {
        return $this->clave;
    }
}