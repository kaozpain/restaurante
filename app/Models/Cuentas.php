<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuentas extends GeneralModel
{
    //
    protected $table='cuentas';
    protected $fillable = ['mesa_historia_id', 'cliente_id', 'fecha_creacion', 'activo'];


    public function Cliente(){
        return $this->belongsTo(Clientes::class, 'cliente_id');
    }

    public function Pedidos(){
        return $this->hasMany(Pedidos::class, 'cuenta_id');
    }

}
