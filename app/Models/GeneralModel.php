<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class GeneralModel extends Authenticatable
{
    const UPDATED_AT = 'fecha_modificacion';
    const CREATED_AT = 'fecha_creacion';

    public function buscar($arguments = [], $where = [], $unscoped = false)
    {
        if(isset($where)){

        }
        if (!$unscoped) {
            $where['activo'] = 1;
        }
        $query=$this::where($where);
        //var_dump($query->toSql());

        return $query->get();
    }

    public function guardar($condicion = [])
    {
        $array = $this->toArray();

        return $this::updateOrCreate($condicion, $array);
    }

    public function guardarMasivo($update = [], $where = [])
    {
        if ($update) {
            if ($where) {
                $this::where($where)->update($update);
            } else {
                $this->update($update);
            }
        } else {
            $this->save();
        }
    }
}
