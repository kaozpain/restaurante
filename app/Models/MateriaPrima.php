<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MateriaPrima extends GeneralModel
{
    //
    protected $table='materia_prima';
    protected $fillable = ['nombre', 'medicion_id', 'cantidad', 'cantidad_alerta','fecha_creacion', 'activo'];

    public function Receta(){
        return $this->belongsTo(Receta::class, 'id','materia_prima_id');
    }

    public function Medicion(){
        return $this->belongsTo(Medicion::class, 'medicion_id');
    }
}
