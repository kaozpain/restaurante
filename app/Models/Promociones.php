<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Promociones extends GeneralModel
{
    //
    protected $table='promociones';
    protected $fillable = ['nombre', 'precio', 'fecha_creacion','activo'];

    public function MenuPromociones(){
        return $this->hasMany(MenuPromociones::class, 'promociones_id');
    }
}
