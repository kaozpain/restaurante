<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MenuPromociones extends GeneralModel
{
    //
    protected $table='menu_promociones';
    protected $fillable = ['promocion_id', 'class', 'class_id'];

    public function Menu(){
        return $this->belongsTo(Menu::class, 'class_id');//->where('class', 'Menu');
    }

    public function Promocion(){
        return $this->belongsTo(Promociones::class, 'promocion_id');//->where('class', 'Menu');
    }

    public function Guarnicion(){
        return $this->belongsTo(Guarnicion::class, 'class_id');//->where('class', 'Guarnicion');
    }
}
