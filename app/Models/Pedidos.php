<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pedidos extends GeneralModel
{
    protected $table='pedidos';
    protected $fillable = ['cuenta_id', 'class', 'class_id', 'entregado', 'activo'];

    public function Menu(){
        return $this->belongsTo(Menu::class, 'class_id','id');
    }

    public function Guarnicion(){
        return $this->belongsTo(Guarnicion::class, 'class_id','id');
    }

    public function Promocion(){
        return $this->belongsTo(Promociones::class, 'class_id','id');
    }

    public function Cuenta(){
        return $this->belongsTo(Cuentas::class, 'cuenta_id','id');
    }

    public function PedidoGuarnicion(){
        return $this->hasMany(PedidosGuarnicion::class, 'pedido_id','id');
    }
}
