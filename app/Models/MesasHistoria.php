<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MesasHistoria extends GeneralModel
{
    //
    protected $table='mesas_historia';
    protected $fillable = ['mesa_id', 'personas', 'multiples_cuentas', 'usuario_id', 'fecha_creacion', 'activo'];

    public function Usuario(){
        return $this->belongsTo(Usuarios::class, 'usuario_id', 'id');
    }

    public function Cuentas(){
        return $this->hasMany(Cuentas::class, 'mesa_historia_id', 'id');
    }
}
