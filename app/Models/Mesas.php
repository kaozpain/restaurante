<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Mesas extends GeneralModel
{
    protected $table='mesas';
    protected $fillable = ['nombre', 'posicion_x', 'posicion_y', 'activo'];

    public function MesasHistoria(){
        return $this->hasMany(MesasHistoria::class, 'mesa_id', 'id');
    }
}
