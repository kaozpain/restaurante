<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Menu extends GeneralModel
{
    //
    protected $table='menu';
    protected $fillable = ['nombre', 'tiene_guarnicion', 'precio', 'fecha_creacion', 'activo'];

    public function TipoMenu(){
        return $this->belongsTo(TipoMenu::class, 'tipo_menu_id');
    }

    public function Receta(){
        return $this->hasMany(Receta::class, 'class_id')->where('class', 'Menu');
    }

    public function MenuGuarnicion(){
        return $this->hasMany(MenuGuarnicion::class, 'menu_id');
    }
}
