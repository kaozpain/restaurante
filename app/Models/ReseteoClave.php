<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReseteoClave extends GeneralModel
{
    //
    protected $table='reseteo_clave';
    protected $fillable = ['email', 'login', 'token'];
}
