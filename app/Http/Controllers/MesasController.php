<?php

namespace App\Http\Controllers;

use App\Models\Cuentas;
use App\Models\MateriaPrima;
use App\Models\MesasHistoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MesasController extends Controller
{
    //
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $scriptJs=$this->initHelperJs();
        $data['scriptsJs']=$scriptJs;
        $materiaPrima= new MateriaPrima();
        $cuentas= new Cuentas();

        $fecha_hoy= date('Y-m-d');
        $fecha_desde= strtotime ( -1 , strtotime ( $fecha_hoy ) ) ;
        $nuevafecha_desde = date ( 'Y-m-d' , $fecha_desde);
        $fecha_hasta= date('Y-m-d');
        //$nuevafecha_hasta = date ( 'Y-m-d' , $fecha_hasta);

        $ventas = $cuentas::from('cuentas as c')->select(DB::raw('IF(promo.nombre,promo.nombre,IF(g.nombre,g.nombre,m.nombre)) as nombre, 
        IF(promo.precio,promo.precio,IF(g.precio,g.precio,m.precio)) as precio,
        sum(p.cantidad) as cantidad, p.class as clase, p.class_id as clase_id'))
            ->join('pedidos as p', 'c.id', '=', 'p.cuenta_id')
            ->leftJoin('menu as m', function ($ljoin){
                $ljoin->on('p.class_id','=','m.id')->where('p.class','=','Menu');
            })
            ->leftJoin('guarnicion as g', function ($ljoin){
                $ljoin->on('p.class_id','=','g.id')->where('p.class','=','Guarnicion');
            })
            ->leftJoin('promociones as promo', function ($ljoin){
                $ljoin->on('p.class_id','=','promo.id')->where('p.class','=','Promocion');
            })
            ->where([['c.fecha_creacion','>=',$fecha_hoy]])->groupBy('clase', 'clase_id','nombre','precio')->get(); //([],[['fecha_creacion','>',$nuevafecha_desde]], true);

        foreach ($ventas as $venta){
            $data['ventas'][]=[
                'nombre'=>$venta['nombre'],
                'cantidad'=> $venta['cantidad'],
                'precio'=> $venta['precio'],
                'precio_final'=> $venta['cantidad']*$venta['precio']];
        }

        if(count($ventas)>0){
            $suma=0;
            foreach ($data['ventas'] as $sumaVentas){
                $suma+=$sumaVentas['precio_final'];
            }
            $data['suma']=$suma;
        }

        $pocoStock = $materiaPrima::from('materia_prima as mp')->join('materia_prima as mp2', function ($join){
            $join->on('mp.id', '=', 'mp2.id');
            $join->on('mp.cantidad', '<=', 'mp2.cantidad_alerta');
        })->where('mp.activo', 1)->get();
        foreach ($pocoStock as $materia){
            $data['Poco_stock'][]=[
                'nombre'=>$materia['nombre'],
                'cantidad'=> $materia['cantidad'].' '.$materia->Medicion['abreviatura'],
            ];
        }

        $sinStock = $materiaPrima->buscar([],[['cantidad','<=', 0]]);
        foreach ($sinStock as $materia){
            $data['sin_stock'][]=[
                'nombre'=>$materia['nombre'],
                'fecha_terminado'=> $materia['fecha_modificacion'],
            ];
        }

        $data['MesasInController']=new MesasInController();

        return view('Informacion/mesas', $data);
    }
}
