<?php

namespace App\Http\Controllers;

use App\Models\Mesas;
use Dotenv\Loader;
use Illuminate\Http\Request;

class MesasInController extends Controller
{
    //
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $libreriasJs=[asset('js/Propias/Mesas.js')];
        $scriptJs=$this->initHelperJs($libreriasJs);
        $data['scriptsJs']=$scriptJs;
        $data['Controller']=&$this;
        return view('Mesas/mesas-info', $data);
    }


    public function getPopUpVista($id_mesa, $nombre){
        $this->initHelperCss();
        $this->passJsVariables();
        $libreriasJs[]=asset('js/Propias/Ordenes.js');
        $scriptJs=$this->initHelperJs($libreriasJs);
        $data['scriptsJs']=$scriptJs;
        $data['id_mesa']=$id_mesa;
        $data['titulo']='Mesa '.$id_mesa;
        $data['nombre']=$nombre;
        $data['MesasInController']=&$this;
        $data['MenuPrincipalController']=new MenuPrincipalController();
        return view('Mesas/popup-mesas-info', $data);
    }

    public function vistaPedido($id){
        $mesaObject= new Mesas();
        $mesa=$mesaObject->buscar([],[['id','=', $id]])->first();
        if(count($mesa)>0){
            foreach ($mesa->MesasHistoria as $mesaHistoria){
                if($mesaHistoria['activo']){
                    $total_mesa=0;
                    foreach ($mesaHistoria->Cuentas as $cuenta){
                        $total_cuenta=0;
                        $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['cliente']['id']=$cuenta->Cliente['id'];
                        $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['cliente']['nombre']=$cuenta->Cliente['nombre'];
                        foreach ($cuenta->Pedidos as $pedido){
                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['id']=$pedido->{$pedido['class']}['id'];
                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['cantidad']=$pedido['cantidad'];
                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['nombre']=$pedido->{$pedido['class']}['nombre'];
                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['precio']=$pedido->{$pedido['class']}['precio'];
                            if($pedido['class'] == "Menu"){
                                foreach ($pedido->PedidoGuarnicion as $pedidoGuarniciones){
                                    $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['guarniciones'][$pedidoGuarniciones['guarnicion_id']]['nombre']
                                        =$pedidoGuarniciones->Guarnicion['nombre'];
                                    $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['guarniciones'][$pedidoGuarniciones['guarnicion_id']]['precio_adicional']
                                        =$pedidoGuarniciones->Guarnicion['precio_adicional'];
                                    $total_cuenta+=$pedidoGuarniciones->Guarnicion['precio_adicional'];
                                }
                            } else if($pedido['class'] == "Promomocion"){
                                foreach ($pedido->Promocion->MenuPromociones as $menuPromocion){
                                    $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['promocion'][$menuPromocion['id']]['cantidad']=
                                        $menuPromocion['cantidad'];
                                    if($menuPromocion['class']=='Menu'){
                                        $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['promocion'][$menuPromocion['id']]['nombre']=
                                            $menuPromocion->Menu['nombre'];
                                        foreach ($pedido->PedidoGuarnicion as $pedidoGuarniciones){
                                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['promocion'][[$menuPromocion['id']]][$pedidoGuarniciones['guarnicion_id']]['nombre']
                                                =$pedidoGuarniciones->Guarnicion['nombre'];
                                            $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['promocion'][[$menuPromocion['id']]][$pedidoGuarniciones['guarnicion_id']]['precio_adicional']
                                                =$pedidoGuarniciones->Guarnicion['precio_adicional'];
                                            $total_cuenta+=$pedidoGuarniciones->Guarnicion['precio_adicional'];
                                        }
                                    } else if($menuPromocion['class']=='Guarnicion'){
                                        $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['pedido'][$pedido['id']]['guarniciones'][$menuPromocion['id']]['nombre']=
                                            $menuPromocion->Guarnicion['nombre'];
                                    }
                                }
                            }
                            $total_cuenta+=($pedido->{$pedido['class']}['cantidad']*$pedido->{$pedido['class']}['precio']);
                            $total_mesa+=$total_cuenta;
                        }
                        $data['tabla'][$mesaHistoria['id']][$cuenta['id']]['cliente']['total_cuenta']=$total_cuenta;
                    }
                    $data['tabla']['total_cuenta']=$total_cuenta;
                }
            }
        }
        $data['orden']=[[
            'plato' => "arroz con menestra y carne",
            'cantidad' => 3,
            'precio_unitario' => 5,
            'precio_total' => 15
        ],
        [
            'plato' => "moro y carne",
            'cantidad' => 2,
            'precio_unitario' => 4,
            'precio_total' => 8
        ]];
        $data['id']=$id;
        return view('Mesas/vista-pedido', $data);
    }

    public function obtenerFilaColumnaMax($mesas){
        foreach ($mesas as $mesa){
            $fila[]=$mesa['posicion_x']+1;
            $columna[]=$mesa['posicion_y']+1;
        }
        return array('fila' => max($fila),'columna' =>  max($columna));
    }

    public function VerPosicionesDeMesas($editable=false)
    {
        //obtener mesas usadas

        $datos=$_POST?:$_GET;
        $data['esMenu']=true;
        $data['creacionMesas']=false;
        if(!$datos){
            $mesas= new Mesas();
            $results=$mesas->buscar();
            if(count($results)>0){
                foreach ($results as $result){
                    $data['mesasTodas'][$result['posicion_x']][$result['posicion_y']]['id']=$result['id'];
                    $data['mesasTodas'][$result['posicion_x']][$result['posicion_y']]['nombre']=$result['nombre'];
                    $data['mesasTodas'][$result['posicion_x']][$result['posicion_y']]['activo']=$result['activo'];
                }
                $dimesion=$this->obtenerFilaColumnaMax($results);
                $datos['columna']=$dimesion['columna'];
                $datos['fila']=$dimesion['fila'];
            } else {
                $datos['columna']=0;
                $datos['fila']=0;
            }
            $data['esMenu']=$editable;
        }
        else {
            echo $datos['columna'];
            echo $datos['fila'];
            $data['creacionMesas']=true;
        }
        $data['columna']=$datos['columna'];
        $data['fila']=$datos['fila'];
        if($datos['fila']){
            $data['tamanio_vertical']=100/$datos['columna'];
            $data['tamanio_horizontal']=100/$datos['fila'];
        }

        return view('Mesas/MenuPosicionMesas', $data);
    }

    public function GuardarPosicionMesas(){
        $datos=$_POST?:$_GET;
        $mesasTodas=$datos['Mesa'];
        $mesaAGuardar=new Mesas();
        $mesaAGuardar->guardarMasivo(['activo' => 0], ['activo' => 1]);
        foreach ($mesasTodas as $fila => $mesas){
            foreach ($mesas as $columna => $mesa){
                $mesaAGuardar['posicion_x']=$fila;
                $mesaAGuardar['posicion_y']=$columna;
                $mesaAGuardar['nombre']=$mesa['nombre']?:"Mesa sin nombre";
                $mesaAGuardar['activo']=$mesa['activo'];
                $mesaAGuardar->guardar(['posicion_x' => $mesaAGuardar['posicion_x'],
                    'posicion_y' => $mesaAGuardar['posicion_y']]);
            }
        }
        return redirect()->route('principal');
    }
}