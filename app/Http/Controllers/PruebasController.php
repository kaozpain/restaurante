<?php

namespace App\Http\Controllers;

use App\Models\Cuentas;
use App\Models\Menu;
use App\Models\Mesas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PruebasController extends Controller
{
    //
    public function index($funcion=null){
        $this->$funcion();
    }

    public function pruebaExtend(){
        $x=new Cuentas();
        $x->buscar();
    }

    public function pruebaModelos(){
        $menu=Mesas::find(1)->first();
        echo($menu->MesasHistoria[0]->Cuentas[0]->Pedidos[0]->MenuGuarnicion->Receta[0]->MateriaPrima->Medicion);
//        var_dump(Mesas::updateOrCreate(['activo' => 1],['posicion_x'=> 1, 'posicion_y' => 1]));
//        echo($menu->receta[0]->materiaPrima->receta);
    }
}
