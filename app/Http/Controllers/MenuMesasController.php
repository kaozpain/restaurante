<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuMesasController extends Controller
{
    //
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $libreriasJs=[asset('js/Propias/Mesas.js')];
        $scriptJs=$this->initHelperJs($libreriasJs);
        $data['scriptsJs']=$scriptJs;
        $data['MesasInController']=new MesasInController();
        return view('Menu/menu-mesas', $data);
    }
}
