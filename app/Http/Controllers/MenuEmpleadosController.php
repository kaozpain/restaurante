<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuEmpleadosController extends Controller
{
    //
    public function index(){
        $this->initHelperCss();
        $this->passJsVariables();
        $scriptJs=$this->initHelperJs();
        $data['scriptsJs']=$scriptJs;
        return view('Menu/menu-empleados', $data);
    }
}
