<?php

namespace App\Http\Controllers\Auth;

use App\Models\Usuarios;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string|max:255',
            'login' => 'required|string|max:255|unique:usuarios',
            'email' => 'string|email|max:255|unique:usuarios',
            'clave' => 'required|string|min:2|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Usuarios
     */
    protected function create(array $data)
    {
        return Usuarios::create([
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'login' => $data['login'],
            'clave' => bcrypt($data['clave']),
            'tipo_usuario_id' => '1',
            'activo' => '1',
        ]);
    }

    /**
     * user registration - show form
     */
    public function showRegistrationForm()
    {
        abort(404);
    }

    /**
     * @inherit
     */
    public function register(Request $request)
    {
        abort(404);
    }
}
