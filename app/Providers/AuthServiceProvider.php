<?php

namespace App\Providers;

use App\Extensions\MyEloquentUserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('usuario', function ($app, array $config) {
            $model = $app['config']['auth.providers.custom_usuario.model'];
            return new MyEloquentUserProvider($app['hash'], $model);
        });
        //
    }
}
