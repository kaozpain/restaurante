let uploader = {
    clickReal: function (id_input) {
        let input=$("#"+id_input);
        input.click();
    },
    previsualizarImagen: function (e) {
        // Creamos el objeto de la clase FileReader
        let reader = new FileReader();
        // Leemos el archivo subido y se lo pasamos a nuestro fileReader
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function() {
            let image = $('#previsualizar_imagen');
            image.attr('src', reader.result);
        }
    }
};